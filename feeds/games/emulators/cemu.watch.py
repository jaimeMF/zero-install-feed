from urllib import request
import json
import re
response = request.urlopen(request.Request('https://cemu.info/changelog.html'))
data = response.read().decode('utf-8')
matches = re.finditer(r'v(?P<version>[\d\.]+) \| (?P<released>\d{4}-\d{2}-\d{2})', data)
releases = [{'version': m.group('version'), 'released': m.group('released')} for m in matches if m.group('released') > '2017-12-13']
