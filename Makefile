CLONE_URL = git@codeberg.org:jaimeMF/zero-install-feed.git
0LAUNCH = 0launch -co

0REPO = $(0LAUNCH) https://apps.0install.net/0install/0repo.xml
0WATCH = $(0LAUNCH) https://apps.0install.net/0install/0watch.xml

update:
	$(0REPO) update

reindex:
	$(0REPO) reindex

checkout-public:
	if [ ! -d public ]; then git clone --branch pages $(CLONE_URL) public; fi
	cd public && git fetch && git pull

publish:
	rsync --include='*/' --include '*.png' --include='*.ico' --exclude='*' --recursive feeds/ public
	cd public && git add --all && git commit -m "Update" && git push

watch:
	find . -iname '*.watch.py' | xargs -n 1 $(0WATCH) --output ./incoming
